let base = document.querySelector('.base')
const firstCase = document.getElementById('first-case')
const boxs = document.querySelectorAll('.case')
const remove = document.querySelector('.delete')
const allCases = []
const choices = []
let pictureInProgress

for(i= 0; i < boxs.length; i++) {
    allCases.push(boxs[i])
}

allCases.push(remove)

let indexPhoto = 1

base.style.backgroundImage = `url(https://loremflickr.com/320/240?random=${indexPhoto})`
pictureInProgress = `url(https://loremflickr.com/320/240?random=${indexPhoto})`

function nvBase(){
    const newBase = document.createElement('div')
    newBase.setAttribute('class', 'base')
    newBase.setAttribute('draggable', 'true')
    indexPhoto++
    newBase.style.backgroundImage = `url(https://loremflickr.com/320/240?random=${indexPhoto})`
    pictureInProgress = `url(https://loremflickr.com/320/240?random=${indexPhoto})`
    firstCase.appendChild(newBase);
    base = newBase;
}


for(const empty of allCases) {
    empty.addEventListener('dragover', dragOver)
    empty.addEventListener('dragenter', dragEnter)
    empty.addEventListener('drop', dragDrop)
}

function dragDrop() {

    if(this.id === "first-case") {
        return
    }

    if(this.id === "delete") {
        base.remove()
        nvBase()
        return
    }

    this.removeEventListener('drop', dragDrop)
    this.removeEventListener('dragenter', dragEnter)
    this.removeEventListener('dragover', dragOver)

    this.appendChild(base)
    this.childNodes[0].setAttribute('draggable', false)
    nvBase()

    choices.push(pictureInProgress)

    if(choices.length === 3) {
        setTimeout(() => {
            alert('Selection terminé')
        }, 200)
    }
}

function dragOver(e) {
    e.preventDefault()
    remove.style.color = 'red'
}

function dragEnter(e) {
    e.preventDefault()
}